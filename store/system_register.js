const system_register = [
  {
    id: "33403185",
    name: "scaffold",
    url: "https://tracksheet.co.uk/pipeline",
    path: "~/apps/ci_cicd_webhook_handler",
    deploy: {
      clone:
        "git clone https://cicdpipe:kySkup-4mavxy-kehved@gitlab.com/cicdpipe/ci_cicd_webhook_handler.git",
      pull: "git stash && git pull https://cicdpipe:kySkup-4mavxy-kehved@gitlab.com/cicdpipe/ci_cicd_webhook_handler.git && pm2 restart app",
    },
  },
];

module.exports = system_register;
